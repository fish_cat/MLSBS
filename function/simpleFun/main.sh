#! Encoding UTF-8


select_simple_fun(){
  if [[ -f ${FUNCTION_PATH}/simpleFun/command.sh ]] ; then
    source ${FUNCTION_PATH}/simpleFun/command.sh
  fi
  echo "----------------------------------------------------------------"
  declare -a VAR_LISTS
  if ${CN} ;then
    echo "[Notice] 请选择小功能:"
    VAR_LISTS=("返回首页" "显示网络")
  else
    echo "[Notice] Which fun :"
    VAR_LISTS=("Back" "Show_NET")
  fi
  select VAR in ${VAR_LISTS[@]} ;do
    case ${VAR} in
      ${VAR_LISTS[1]})
        my_ip;;
      ${VAR_LISTS[0]})
        main;;
      *)
        select_simple_fun;;
    esac
    break
  done
}

select_simple_fun