#! Encoding UTF-8

#check system parameter about cpu's core ,ram ,other
#
SYS_NAME=""
SYS_COUNT=""
FILE_MAX=$(cat /proc/sys/fs/file-max)
OS_LIMIT=$(ulimit -n)
egrep -i "centos" /etc/issue && SYS_NAME='centos'
egrep -i "debian" /etc/issue && SYS_NAME='debian'
egrep -i "ubuntu" /etc/issue && SYS_NAME='ubuntu'
SYS_VERSION=$(uname -r|cut -d. -f1-2)
SYS_BIT='32' && [ $(getconf WORD_BIT) == '32' ] && [ $(getconf LONG_BIT) == '64' ] && SYS_BIT='64'

