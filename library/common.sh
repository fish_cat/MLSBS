#! Encoding UTF-8

## 输出打印信息
tmp_msg(){
  if [[ -z $2 ]] ; then
    TMP_MSG_RESULT="$1"
  else 
    ${CN} && TMP_MSG_RESULT="$1" || TMP_MSG_RESULT="$2"
  fi
}

## 10秒自动下一步，给时间选择的同时，不要阻断脚本的运行
pass_enter_to_exit(){
  tmp_msg "请按回车后继续，否则10秒内自动下一步。" "enter or wait 10s to continue"
  read -p "${TMP_MSG_RESULT}" -t 10 OK_RESULT
  echo ""
}

## 检查文件
test_file(){
  if [[ ! -f $1 ]];then
    info_log "没有发现 $1 文件" "Not exist $1"
    pass_enter_to_exit
    return 1
  else
    info_log "正在读取文件 $1" "loading $1 now..."
    return 0
  fi
}

## 检查程序
test_programs(){
  for ARG do
    if [[ -z $(which ${ARG}) ]];then
      info_log "系统上缺少程序 ${ARG}" "Your system do not have ${ARG}"
      return 1
    else
      info_log "正在加载程序 ${ARG} ..." "loading ${ARG} ..."
      return 0
    fi
  done
}

## 退回主页
back_to_index(){
  if [[ $? -gt 0 ]];then
    info_log "准备返回首页" "Ready back to index"
    pass_enter_to_exit
    main
  else
    info_log "成功，继续下一步。" "succeed , continue ..."
  fi
}

## 输入选择
input_choose(){
  VAR_TMP=
  select VARS in $@ "exit"; do
    case ${VARS} in
      ${VARS})
        [[ "${VARS}" == "exit" ]] \
          && VAR_TMP="" \
          || VAR_TMP="${VARS}"
        break;;
    esac
    info_log "请重新选择." "Input again"
  done
}

#检测是否root账号
test_root(){
  [[ $(id -u) != '0' ]] \
    && exit_msg "请使用root账号运行此程序." "Please use root to run this script."
}

#install some tool
#
#这功能可以给其他脚本调用来安装基础软件，如编译mysql需要的gcc等工具
#
install_base_packages(){
  if [[ "${SYS_NAME}" == 'centos' ]]; then
    echo '[yum-fastestmirror Installing] ************************************************** >>';
    [[ -z $SYS_COUNT ]] \
      && yum -y install yum-fastestmirror \
      && SYS_COUNT="1"
    cp -f /etc/yum.conf /etc/yum.conf.back
    sed -i 's:exclude=.*:exclude=:g' /etc/yum.conf
    for ARG do
      info_log \
        "正在安装 ${ARG} ************************************************** >>" \
        "[${ARG} Installing] ************************************************** >>";
      yum -y install ${ARG}; 
    done;
    mv -f /etc/yum.conf.back /etc/yum.conf;
  else
    [[ -z $SYS_COUNT ]] \
      && apt-get update \
      && SYS_COUNT="1"
    apt-get -fy install;apt-get -y autoremove --purge;
    dpkg -l \
      | grep ^rc \
      | awk '{print $2}' \
      | sudo xargs dpkg -P 
    for ARG do
      info_log \
        "正在安装 ${ARG} ************************************************** >>" \
        "[${ARG} Installing] ************************************************** >>";
      apt-get install -y ${ARG} --force-yes;
    done;
  fi;
  return 1
}

install_base_cmd(){
  declare TMP_COUNT
  declare -a PROGRAMS_LIST
  TMP_COUNT=0
  for ARG do
    test_programs ${ARG}
    if [[ $? -eq 1 ]];then
      PROGRAMS_LIST[${TMP_COUNT}]=${ARG}
      TMP_COUNT=$[${TMP_COUNT}+1]
    fi
  done
  info_log \
    "正在安装这些程序:${PROGRAMS_LIST[@]}" \
    "Setting up these programs : ${PROGRAMS_LIST[@]}"
  install_base_packages ${PROGRAMS_LIST[@]}
}

## 打包程序
packet_tools(){
  declare CMD_SCRIPT
  declare INSTALL_TOOLS_SCRIPT

  info \
    "正在打包,请稍后..." \
    "Are packaged, please later..."
  CMD_SCRIPT="$1"
  INSTALL_TOOLS_SCRIPT="$2"
  #检测需要的程序
  test_programs "gzexe" "tar"
  test_file "${CMD_SCRIPT}"
  test_file "${INSTALL_TOOLS_SCRIPT}"
  [[ ! -d ${SCRIPT_PATH}/packet ]] \
    && mkdir -p ${SCRIPT_PATH}/packet
  cp $CMD_SCRIPT ${DOWNLOAD_PATH}/
  CMD_SCRIPT_NAME=$(basename $CMD_SCRIPT)
  INSTALL_TOOLS_SCRIPT_NAME=${INSTALL_TOOLS_SCRIPT_NAME}
  cd "${DOWNLOAD_PATH}/" \
    && gzexe "${CMD_SCRIPT_NAME}" \
    && tar -zcf "${CMD_SCRIPT_NAME%%.*}" "${CMD_SCRIPT_NAME}" \
    && cat ${INSTALL_TOOLS_SCRIPT} "${CMD_SCRIPT_NAME%%.*}" \
    > ${SCRIPT_PATH}/packet/${INSTALL_TOOLS_SCRIPT_NAME} \
    && cd ${SCRIPT_PATH}/packet/ \
    && gzexe ${INSTALL_TOOLS_SCRIPT_NAME} \
    && mv ${DOWNLOAD_PATH}/${INSTALL_TOOLS_SCRIPT_NAME} ${SCRIPT_PATH}/packet/${INSTALL_TOOLS_SCRIPT_NAME} \
    && rm -f ${DOWNLOAD_PATH}/${CMD_SCRIPT_NAME}* \
    && rm -f ${SCRIPT_PATH}/packet/${INSTALL_TOOLS_SCRIPT_NAME}~ \
    || exit_msg "程序生成失败!" "The corresponding create false!"

  info_log \
    "安装包已生成,路径为:${SCRIPT_PATH}/packet/${INSTALL_TOOLS_SCRIPT_NAME}" \
    "The installation package has been generated, the path is : ${SCRIPT_PATH}/packet/${INSTALL_TOOLS_SCRIPT_NAME}"
}