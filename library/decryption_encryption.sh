#! Encoding UTF-8
encry_gzexe(){
  test_programs gzexe
  [ $? -eq 0 ] && gzexe $1
  rm -f $1~
}

shc_install(){
  SHC_VERSION="shc-3.8.9"
  info_log "正在安装 ${SHC_VERSION} ..." "Install ${SHC_VERSION} ..."
  cd /tmp/
  [ ! -f ${SHC_VERSION}.tgz ] && wget http://www.datsi.fi.upm.es/~frosal/sources/${SHC_VERSION}.tgz
  tar vxf /tmp/${SHC_VERSION}.tgz
  cd /tmp/${SHC_VERSION}
  test_programs gcc
  [ $? -eq 1 ] && INSTALL_BASE_PACKAGES gcc
  make test
  make strings
  make expiration
  [ ! -d /usr/local/man/man1/ ] && mkdir -p /usr/local/man/man1/
  make install <<EOP
y
EOP
}

encry_shc(){
  test_programs shc
  [ $? -eq 1 ] && shc_install
  CFLAGS=-static shc -r -f $1
  rm -rf $1 $1~ $1.x.c
  mv $1.x $1
}

select_encry_function(){
  info "----------------------------------------------------------------"
  declare -a VarLists
  if ${CN} ;then
    info "[Notice] 请选择需要你想要加密脚本的方式:"
    VarLists=("使用gzexe压缩" "使用shc加密" "不加密")
  else
    info "[Notice] How to encryption your script:"
    VarLists=("Use_gzexe" "Use_shc" "Do_not_encryption")
  fi
  select var in ${VarLists[@]} ;do
    case $var in
      ${VarLists[0]})
        ENCRY_FUNCTION="encry_gzexe";;
      ${VarLists[1]})
        ENCRY_FUNCTION="encry_shc";;
      ${VarLists[2]})
        ENCRY_FUNCTION="";;
      *)
        select_encry_function;;
    esac
    break
  done
}

md5_string(){
  echo "$@" \
    | md5sum \
    | head -c 8
}