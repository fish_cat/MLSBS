#!/usr/bin/env bash
#! Encoding UTF-8
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH
clear;

SCRIPT_PATH=$(cd $(dirname "$0") && pwd)

init(){
  ## 加载配置内容
  [[ ! -f ${SCRIPT_PATH}/config.sh ]] \
    && echo "not config: ${SCRIPT_PATH}/config.sh" \
    && exit 1
  source ${SCRIPT_PATH}/config.sh
  [[ ! -d ${SCRIPT_PATH}/init ]] \
    && echo "not init: ${SCRIPT_PATH}/init" \
    && exit 1
  for ARG in $(echo ${SCRIPT_PATH}/init/*.sh) ; do 
    [[ -f ${ARG} ]] && source ${ARG}
  done
}
## 初始化
init

[[ "${SYS_NAME}" == '' ]] \
  && exit_msg \
    "程序不支持在此系统上运行。" \
    "Your system is not supported this script"

library(){
  if [[ ! -d ${LIB_PATH} ]] ; then
    info "not library: ${LIB_PATH}" 
  else
    for ARG in $(echo ${LIB_PATH}/*.sh) ; do 
      [[ -f ${ARG} ]] && source ${ARG}
    done
  fi
}

# 加载库
library

#main
main(){
  info "----------------------------------------------------------------"
  info_log "[Notice] 请选择要运行的指令:" "[Notice] Which function you want to run:"
  declare -A VAR_FUN_LISTS

  for VAR in $(echo ${FUNCTION_PATH}/*) ; do 
    if [[ ! -d ${VAR} ]] ; then
      continue
    fi
    if [[ ! -f ${VAR}/name.conf ]] ; then
      continue
    fi
    if [[ ! -f ${VAR}/main.sh ]] ; then
      continue
    fi
    CN_NAME=""
    EN_NAME=""
    FUN_NAME="$(basename ${VAR})"
    VAR_NAME=""
    source ${VAR}/name.conf
    ${CN} && VAR_NAME=${CN_NAME} || VAR_NAME=${EN_NAME}
    [[ -z ${VAR_NAME} ]] && VAR_NAME=${FUN_NAME}

    VAR_FUN_LISTS[${VAR_NAME}]=${VAR}
  done

  select VAR in ${!VAR_FUN_LISTS[@]};do
    case ${VAR} in
      ${VAR})
        info_log "fun ${VAR}"
        source ${VAR_FUN_LISTS[${VAR}]}/main.sh;;
      *)
        main;;
    esac
    break
  done
  main
}

main
